import { NgModule } from '@angular/core';

// modules
import { CommonModule } from '@angular/common';

// vex
import { LayoutModule } from '@vex-mgx/layout/layout.module';
import { SidenavModule } from '@vex-mgx/layout/sidenav/sidenav.module';
import { ToolbarModule } from '@vex-mgx/layout/toolbar/toolbar.module';
import { FooterModule } from '@vex-mgx/components/footer/footer.module';
import { ConfigPanelModule } from '@vex-mgx/components/config-panel/config-panel.module';
import { SidebarModule } from '@vex-mgx/components/sidebar/sidebar.module';
import { QuickpanelModule } from '@vex-mgx/components/quickpanel/quickpanel.module';

// components
import { CustomLayoutComponent } from '@custom-layout-mgx/custom-layout.component';


@NgModule({
  declarations: [CustomLayoutComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SidenavModule,
    ToolbarModule,
    FooterModule,
    ConfigPanelModule,
    SidebarModule,
    QuickpanelModule
  ]
})
export class CustomLayoutModule {
}
